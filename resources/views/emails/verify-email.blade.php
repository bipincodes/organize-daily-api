Greetings {{$data['name']}}, <br/>
<b>Congratulations for starting an initiative to organize your day.</b><br/>
To start add this code <h1>{{$data['verification_link'] }}</h1> to verify your email and start organizing your life like never before. <br/>
<br/>
Regards, <br/>
Organize Daily