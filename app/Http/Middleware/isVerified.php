<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use App\Models\User;

class isVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $api = $request->header('Authorization');
        $user = User::where('api_secret',$api)->first();
        if(!$user){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "cannot allow you inside the system";
            return response()->json($json_resp);
        }
        return $next($request);
    }
}
