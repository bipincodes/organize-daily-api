<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;

use App\Models\User;
use App\Models\PremiumPlan;

class isPremiumActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $api = $request->header('Authorization');
        if(!$api){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "cannot allow you inside the system";
            return response()->json($json_resp);
        }
        $user = User::where('api_secret',$api)->first();
        if(!$user){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "cannot validate the user";
            return response()->json($json_resp);
        }

        /** get premium plan of the user */
        $today = Carbon::now();
        $premium = PremiumPlan::where('user_id',$user->id)->first();
        $endDate = Carbon::parse($premium->end_date);
        $result = $today->gt($endDate);
        if($result){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "your premium access is expired";
            return response()->json($json_resp);
        }
        return $next($request);
    }
}
