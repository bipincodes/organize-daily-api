<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\WhatsWhere;

class WhatsWhereController extends Controller
{

    public function __construct(WhatsWhere $whatswhere){
        $this->whatswhere =$whatswhere;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $whatswhere = $this->whatswhere->where('user_id',$userID)->get();
        if($whatswhere->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any entry on the record";
            return response()->json($json_resp);
        }
        $whereData =[];
        foreach($whatswhere as $saman){
            $whereData[] = [
                'id' => $saman->id,
                'what' => $saman->what,
                'where' => $saman->where,
                'created' => $saman->created_at->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your to-do list";
        $json_resp['data'] = [
            'whatwhere' => $whereData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'what' => 'required',
            'where' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data = [
            'what' => $request->input('what'),
            'where' => $request->input('where'),
            'user_id' => $userID,
        ];
        $whatsWhere = new WhatsWhere;
        $newData = $whatsWhere->add_data($data);
        if($newData['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the entry successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the entry.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'what' => 'required',
            'where' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data = [
            'what' => $request->input('what'),
            'where' => $request->input('where'),
        ];
        $whatswhere = new WhatsWhere;
        $updatewhatswhere = $whatswhere->update_data($id,$data,$userID);
        if($updatewhatswhere == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the entry";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request$request,$id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $whatswhere = new WhatsWhere;
        $delwhatswhere = $whatswhere->delete_data($id,$userID);
        if($delwhatswhere ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the entry";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete the entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
