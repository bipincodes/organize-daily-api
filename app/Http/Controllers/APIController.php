<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings;

class APIController extends Controller
{
    public function yo(){
        $json_resp['status'] = 'success';
        $json_resp['message'] = 'If you see this response, Organize Daily is working!';
        return response()->json($json_resp);
    }

    public function getVAVersion(){
        $version = Settings::where('key','va_version')->first();
        $json_resp['status'] = 'success';
        $json_resp['data'] = $version->value;
        return response()->json($json_resp);
    }
}
