<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;


use Validator;

use App\Models\User;
use App\Models\PremiumPlan;

use App\Mail\VerifyEmail;
// use App\Mail\ResetPassword;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function __construct(User $users){
        $this->users = $users;
    }


    public function registerUser(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'nickname' => 'required',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'password' => 'required',
            'confirm_password' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        /** check if password matches */
        if($request->password !== $request->confirm_password){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "confirm password does not match";
            return response()->json($json_resp);
        }
        /** check if email and phone aren't register in the system */
        $emailTaken = $this->users->where('email',$request->email)->first();
        $phoneTaken = $this->users->where('phone',$request->phone)->first();
        if($emailTaken != null && $phoneTaken != null){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "email or phone exists on the system";
            return response()->json($json_resp);
        }
        /** register new user in the system */
        $user = new User;
        $data = [
            'name' => $request->name,
            'nickname' => $request->nickname,
            'email' => $request->email,
            'phone' => (int)$request->phone,
            'password' => $request->password,
        ];
        $addUser = $user->add_data($data);
        if($addUser['status'] == true){
            $code = rand(0, 99999999);
            $codeStatus = $user->add_verification_code($addUser['id'],$code);
            /** send verification code on email */
            $mailData = [
                'email' => $request->email,
                'name' => $request->nickname,
                'verification_link' => $code
            ];
            $status = Mail::to($mailData['email'])->send(new VerifyEmail($mailData));

            $data = [
                'user_id' => $addUser['id'],
                'start_date' => Carbon::now(),  
                'end_date' => Carbon::now()->addDay(3),  
            ];

            $premium = new PremiumPlan;
            $premium_data = $premium->add_data($data);

            $json_resp['status'] = 'success';
            $json_resp['message'] = "a verification code has been sent to your email";
            $json_resp['data'] = [
                'user_id' => $addUser['id']
            ];
            return response()->json($json_resp);
        }
    }

    public function verifyAccount(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id' => 'required',
            'code' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        
        /** check if verification code matches */
        $user = $this->users->where('id',$request->user_id)->where('verification_code',$request->code)->first();
        if(!$user){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "cannot validate the user";
            return response()->json($json_resp);
        }
        $user->status = 1;
        $user->verification_code = null;
        $status = $user->update();
        if($status){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "account is validated successfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "i cannot verify your account at the moment";
        }
        return response()->json($json_resp);
    }

    public function loginUser(Request $request){
        $validate = Validator::make($request->all(), [
            'phone' => 'required|numeric|digits:10',
            'password' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        /** check if user exists on the system */
        $user = $this->users->where('phone',$request->phone)->first();
        if(!$user){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "user not found on the system";
            return response()->json($json_resp);
        }
        /** check if user is verified */
        if($user->status == 0 ){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "account is not verified";
            return response()->json($json_resp);
        }
        $credentials = [
            'phone' => $request->input('phone'),
            'password' => $request->input('password'),
        ];
        if (Auth::attempt($credentials)) {
            $json_resp['status'] = 'success';
            $json_resp['message'] = "user logged in successful";
            $json_resp['data'] = [
                'api_key' => $user->api_secret 
            ];
            return response()->json($json_resp);
        }
        $json_resp['status'] = 'error';
        $json_resp['message'] = "credential does not match";
        return response()->json($json_resp);
    }

}
