<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\TodoList;

class TodoController extends Controller
{
    public function __construct(TodoList $lists){
        $this->lists = $lists;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $todo = $this->lists->where('user_id',$userID)->orderBy('created_at','DESC')->get();
        if($todo->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any to-do list";
            return response()->json($json_resp);
        }
       
        $todoData =[];
        foreach($todo as $list){
            $todoData[] = [
                'id' => $list->id,
                'title' => $list->title,
                'task_done' => $list->task_done,
                'created' => $list->created_at->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your to-do list";
        $json_resp['data'] = [
            'todo' => $todoData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'user_id' => $userID,
            'title' => $request->title
        ];
        $todo = new TodoList;
        $newTodo = $todo->add_data($data);
        if($newTodo['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the to-do entry successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the to-do list.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'title' => $request->title
        ];
        $todo = new TodoList;
        $updateTodo = $todo->update_data($id,$data,$userID);
        if($updateTodo == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the to-do list";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the todo entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $todo = new TodoList;
        $delTodo = $todo->delete_data($id,$userID);
        if($delTodo ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the to-do list";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete the todo entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
