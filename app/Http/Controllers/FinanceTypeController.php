<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\FinanceType;

use Validator;

class FinanceTypeController extends Controller
{

    public function __construct(FinanceType $types){
        $this->types = $types;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $types = $this->types->where('user_id',$userID)->orderBy('id','ASC')->get();
        if($types->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any financies type";
            return response()->json($json_resp);
        }
        $typeData = [];
        foreach($types as $type){
            $typeData[] =[
                'id' => $type->id,
                'type' => $type->type,
                'created' => $type->created_at->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your finances type";
        $json_resp['data'] = [
            'type' => $typeData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'user_id' => $userID,
            'type' => $request->type,
        ];
        $types = new FinanceType;
        $newtypes = $types->add_data($data);
        if($newtypes['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the type successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the type.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'type' => $request->type,
        ];
        $types = new FinanceType;
        $updatetypes = $types->update_data($id,$data,$userID);
        if($updatetypes == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the type";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the type. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $types = new FinanceType;
        $deltypes = $types->delete_data($id,$userID);
        if($deltypes ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the financial type record";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete financial type record. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
