<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\Finance;

class FinanceController extends Controller
{
    public function __construct(Finance $finances){
        $this->finances = $finances;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $financeRecord = $this->finances->where('user_id',$userID)->orderBy('id','DESC')->get();
        if($financeRecord->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any financial records";
            return response()->json($json_resp);
        }
        $financeData = [];
        foreach($financeRecord as $record){
            $financeData[] =[
                'id' => $record->id,
                'reason' => $record->reason,
                'amount' => $record->amount,
                'type' => $record->type,
                'tag' => $record->tag_id,
                'created' => $record->created_at->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your financial record";
        $json_resp['data'] = [
            'finance' => $financeData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'reason' => 'required',
            'amount' => 'required',
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        if($request->tag != null){
            $tag = $request->tag;
        }else{
            $tag = null;
        }
        $data =[
            'user_id' => $userID,
            'reason' => $request->reason,
            'amount' => $request->amount,
            'type' => $request->type,
            'tag' => $tag,
        ];
        $finance = new Finance;
        $newfinance = $finance->add_data($data);
        if($newfinance['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the finance record successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the finance record.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'reason' => 'required',
            'amount' => 'required',
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        if($request->tag){
            $tag = $response->tag;
        }else{
            $tag = null;
        }
        $data =[
            'reason' => $request->reason,
            'amount' => $request->amount,
            'type' => $request->type,
            'tag' => $tag,
        ];
        $finance = new Finance;
        $updatefinance = $finance->update_data($id,$data,$userID);
        if($updatefinance == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the finance record";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the finance record. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $finance = new Finance;
        $delfinance = $finance->delete_data($id,$userID);
        if($delfinance ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the entry";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete the entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
