<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\FinancesTag;


class FinancesTagController extends Controller
{
    public function __construct(FinancesTag $tags){
        $this->tags = $tags;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        if($tags->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any financies tags";
            return response()->json($json_resp);
        }
        $tagData = [];
        foreach($tags as $tag){
            $tagData[] =[
                'id' => $tag->id,
                'name' => $tag->name,
                'created' => $tag->created_at->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your finances tags";
        $json_resp['data'] = [
            'tags' => $tagData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'user_id' => $userID,
            'name' => $request->name,
        ];
        $tags = new FinancesTag;
        $newtags = $tags->add_data($data);
        if($newtags['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the tag successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the tag.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'name' => $request->name,
        ];
        $tags = new FinancesTag;
        $updatetags = $tags->update_data($id,$data,$userID);
        if($updatetags == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the tag";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the tag. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $tags = new FinancesTag;
        $deltags = $tags->delete_data($id,$userID);
        if($deltags ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the to-do list";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete the todo entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
