<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PremiumPlan;
use Illuminate\Support\Carbon;

class PremiumController extends Controller
{
    public function __construct(PremiumPlan $plans){
        $this->plans = $plans;
    }

    public function renewPremium(Request $request,$uid){
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $plan = $this->plans->where('user_id',$uid)->get();
        if(!$plan){
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot find any premium record of this user';
            return response()->json($json_resp);
        }
        $data = [
            'user_id' => $userID,
            'start_date' => Carbon::now(),  
            'end_date' => Carbon::now()->addYear(),  
        ];
        $premium = new PremiumPlan;
        $premium_data = $premium->add_data($data);
        if($premium_data['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = 'I renewed the premium for this user';
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot renew premium for this user';
        }
        return response()->json($json_resp);
    }

    public function getPremium(){
        $premiums = $this->plans->join('users', 'users.id', '=', 'premium_plans.user_id')->get();
        if($premiums->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = 'I cannot find any premium records'; 
            return response()->json($json_resp);
        }
        $premiumData = [];
        foreach($premiums as $premium){
            $premiumData[] =[
                'name' => $premium->name,
                'email' => $premium->email,
                'phone' => $premium->phone,
                'start_date' => Carbon::parse($premium->start_date)->diffForHumans(),
                'end_date' => Carbon::parse($premium->end_date)->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your premium records";
        $json_resp['data'] = [
            'premium' => $premiumData
        ];
        return response()->json($json_resp);
    }
}
