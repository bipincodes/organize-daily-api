<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\EmailList;
use App\Mail\WelcomeEmailList;
use Illuminate\Support\Facades\Mail;

class EmailListController extends Controller
{
    public function __construct(EmailList $lists){
        $this->lists = $lists;
    }
    public function addEmailList(Request $request){
        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $list = $this->lists->where('email',$request->email)->first();
        if($list){
            $list->is_subscribed = 1;
            $status = $list->update();
            if($status){
                /** send email to the subscribe thank you */
                $mailData = [
                    'email' => $request->email,
                ];
                $json_resp['status'] = 'success';
                $json_resp['message'] = 'Thank you for joining our Email List';
                return response()->json($json_resp); 
            }else{
                $json_resp['status'] = 'error';
                $json_resp['message'] = 'Something went wrong';
                return response()->json($json_resp); 
            }
        }
        $data = [
            'email' => $request->email
        ];
        $emailList = new EmailList;
        $new_emailList = $emailList->add_data($data);
        if($new_emailList['status']){
            /** send email to the subscribe thank you */
            $json_resp['status'] = 'success';
            $json_resp['message'] = 'Thank you for joining our Email List';
            return response()->json($json_resp); 
        }
        $json_resp['status'] = 'error';
        $json_resp['message'] = 'Something went wrong';
        return response()->json($json_resp); 
    }

    public function deleteEmailList($email){
        $CheckEmail = $this->lists->where('email',$email)->first();
        if($CheckEmail->first()){
            $data = [
                'email' => $email
            ];
            $emailList = new EmailList;
            $delete_emailList = $emailList->delete_data($data);
            if($delete_emailList){
                echo "Sorry to see you go!";die;
            }
            echo "Something went wrong";die;
    
        }
        echo "Sorry to see you go!";die;
    }

    public function addEmailFromSystem(Request $request){
        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validate->fails()) {
            return redirect('/?email=error');
        }
        $list = $this->lists->where('email',$request->email)->first();
        if($list){
            $list->is_subscribed = 1;
            $status = $list->update();
            if($status){
                return redirect('?email=success'); 
            }else{
                return redirect('?email=error');
            }
        }
        $data = [
            'email' => $request->email
        ];
        $emailList = new EmailList;
        $new_emailList = $emailList->add_data($data);
        if($new_emailList['status']){
            return redirect('/?email=success'); 
        }
        return redirect('/?email=error');
    }
}
