<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\Subscription;

use Illuminate\Support\Carbon;

class SubscriptionController extends Controller
{
    public function __construct(Subscription $subscriptions){
        $this->subscriptions = $subscriptions;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $subscriptions = $this->subscriptions->where('user_id',$userID)->orderBy('created_at','DESC')->get();
        if($subscriptions->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any subscriptions";
            return response()->json($json_resp);
        }
       
        $subscriptionsData =[];
        foreach($subscriptions as $subscription){
            $subscriptionsData[] = [
                'id' => $subscription->id,
                'title' => $subscription->title,
                'subscribed_date' => Carbon::parse($subscription->subscribed_date)->diffForHumans(),
                'end_date' => Carbon::parse($subscription->end_date)->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your subscriptions";
        $json_resp['data'] = [
            'subscription' => $subscriptionsData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'start_date' => 'required',
            'month' => 'required'
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
    
        $startDate = Carbon::parse($request->start_date);
        $data =[
            'user_id' => $userID,
            'title' => $request->title,
            'start_date' => Carbon::parse($request->start_date),
            'end_date' => $startDate->addMonths($request->month),
        ];
        $subscription = new Subscription;
        $newsubscription = $subscription->add_data($data);
        if($newsubscription['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the subscription successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the subscription.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'start_date' => 'required',
            'month' => 'required'
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $startDate = Carbon::parse($request->start_date);
        $data =[
            'title' => $request->title,
            'start_date' => Carbon::parse($request->start_date),
            'end_date' => $startDate->addMonths($request->month),
        ];
        $subscription = new Subscription;
        $updatesubscription = $subscription->update_data($id,$data,$userID);
        if($updatesubscription == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the subscription entry";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the subscription entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $subscription = new Subscription;
        $delsubscription = $subscription->delete_data($id,$userID);
        if($delsubscription ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the to-do list";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete the todo entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
