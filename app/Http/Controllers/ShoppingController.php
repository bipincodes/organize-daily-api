<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\ShoppingList;

class ShoppingController extends Controller
{
    public function __construct(ShoppingList $shoppingLists){
        $this->shoppingLists = $shoppingLists;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $shoppingLists = $this->shoppingLists->where('user_id',$userID)->orderBy('id','DESC')->get();
        if($shoppingLists->isEmpty()){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I cannot find any shopping list";
            return response()->json($json_resp);
        }
        $shoppingData = [];
        foreach($shoppingLists as $list){
            $shoppingData[] =[
                'id' => $list->id,
                'item' => $list->item,
                'is_bought' => $list->is_bought,
                'created' => $list->created_at->diffForHumans()
            ];
        }
        $json_resp['status'] = 'success';
        $json_resp['message'] = "These are your shopping list";
        $json_resp['data'] = [
            'shopping' => $shoppingData
        ];
        return response()->json($json_resp);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'item' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'user_id' => $userID,
            'item' => $request->item,
        ];
        $shopping = new ShoppingList;
        $newshopping = $shopping->add_data($data);
        if($newshopping['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the shopping list successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the shopping list.something is wrong';
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'item' => 'required',
            'is_bought' => 'required'
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'item' => $request->item,
            'is_bought' => $request->is_bought
        ];
        $shopping = new ShoppingList;
        $updateshopping = $shopping->update_data($id,$data,$userID);
        if($updateshopping == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the shopping entry";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the shopping entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $shopping = new ShoppingList;
        $delshopping = $shopping->delete_data($id,$userID);
        if($delshopping ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I deleted the to-do list";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot delete the todo entry. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }

    public function tickItem(Request $request,$id){
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $shopping = new ShoppingList;
        $updateShopping = $shopping->tick_untick_shopping_list($id,$userID);
        if($updateShopping ==true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the shopping list";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot update the shopping list. I might be malfunctioning";
        }
        return response()->json($json_resp);
    }
}
