<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use App\Models\FeedBack;

class FeedbackController extends Controller
{
    public function postFeedback(Request $request){
        $apiSecret = $request->header('Authorization');
        $userID = get_uid_from_api_key($apiSecret);
        $validate = Validator::make($request->all(), [
            'feedback' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data =[
            'user_id' => $userID,
            'feedback' => $request->feedback,
        ];
        $shopping = new FeedBack;
        $newshopping = $shopping->add_data($data);
        if($newshopping['status'] == true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the feedback successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = 'I cannot add the feedback.something is wrong';
        }
        return response()->json($json_resp);
    }
}
