<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\Settings;

class SettingsController extends Controller
{
    public function __construct(Settings $settings){
        $this->settings = $settings;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'key' => 'required',
            'value' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        /** check if key already exists */
        $checkKey = $this->settings->where('key',$request->key)->first();
        if($checkKey){
            $json_resp['status'] = 'error';
            $json_resp['message'] = "Same Key already exists";
            return response()->json($json_resp);
        }
        $setting = new Settings;
        $data = [
            'key' => $request->key,
            'value' => $request->value
        ];
        $new = $setting->add_data($data);
        if($new['status'] ==  true){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I added the settings data successsfully";
        }else{
            $json_resp['status'] = 'error';
            $json_resp['message'] = "I cannot add the settings data";
        }
        return response()->json($json_resp);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'value' => 'required',
        ]);
        if ($validate->fails()) {
            $json_resp['status'] = 'error';
            $json_resp['message'] = $validate->errors()->all();
            return response()->json($json_resp);
        }
        $data = [
            'id' => $id,
            'value' => $request->value
        ];
        $setting = new Settings;
        $update = $setting->update_data($data);
        if($update){
            $json_resp['status'] = 'success';
            $json_resp['message'] = "I updated the settings data successsfully";
            return response()->json($json_resp);
        }
        $json_resp['status'] = 'error';
        $json_resp['message'] = "I cannot update the settings data";
        return response()->json($json_resp);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
