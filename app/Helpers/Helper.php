<?php
    use App\Models\User;


    function create_a_dummy_user(){
        $user = User::factory()->count(1)->create();
        return $user[0]->id;
    }

    function get_uid_from_api_key($api){
        $user = User::where('api_secret',$api)->first();
        if(!$user){
            return false;
        }
        return $user->id;
    }