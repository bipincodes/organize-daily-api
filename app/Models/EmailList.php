<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailList extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'email' => $data['email']
        ]);
        if($status->id){
            return ['status'=> true,'id' => $status->id];
        }
        return ['status'=> false, 'api_secret' => null];
    }

    public function delete_data($data){
        $record = $this->where('email',$data['email'])->first();
        if(!$record->first()){
            return false;
        }
        $record->is_subscribed = 0;
        $status = $record->update();
        return $status;
    }
}
