<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancesTag extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'name' => $data['name'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $tags = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$tags){
            return false;
        }
        $tags->name = $data['name'];
        $status = $tags->update();
        return $status;  
    }

    public function delete_data($id,$uid){
        $tag = $this->where('id',$id)->where('user_id',$uid)->first(); 
        if(!$tag){
            return false;
        }
        $status = $tag->delete();
        return $status;
    }
}
