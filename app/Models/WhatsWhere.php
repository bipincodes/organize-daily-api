<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsWhere extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'what' => $data['what'],
            'where' => $data['where'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $whatsWhere = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$whatsWhere){
            return false;
        }
        $whatsWhere->what = $data['what'];
        $whatsWhere->where = $data['where'];
        $status = $whatsWhere->update();
        return $status;
    }

    public function delete_data($id,$uid){
        $whatsWhere = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$whatsWhere){
            return false;
        }
        $status = $whatsWhere->delete();
        return $status;
    }
}
