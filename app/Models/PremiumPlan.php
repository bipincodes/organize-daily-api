<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremiumPlan extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $premium = $this->where('user_id',$data['user_id'])->first();
        if(!$premium){
            $status = $this->create([
                'user_id' => $data['user_id'],
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
            ]);
            if($status->id){
                return ['status'=> true, 'id' => $status->id];
            }
            return ['status'=> false, 'id' => null];
        }else{
            $premium->start_date = $data['start_date'];
            $premium->end_date = $data['end_date'];
            $status = $premium->update();
            return [ 'status' => $status, 'id' =>$premium->id];
        }
    }
}
