<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;


use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'nickname',
        'password',
        'phone',
        'api_secret',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function add_data($data){
        $status = $this->create([
            'name' => $data['name'],
            'nickname' => $data['nickname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
            'api_secret' => Str::random(50)
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function add_verification_code($uid,$code){
        $user = $this->where('id',$uid)->first();
        if(!$user){
            return false;
        }
        $user->verification_code = $code;
        $status = $user->update();
        return $status;
    }

    public function update_data($uid,$data){

    }

    public function change_password($uid,$password){
        $user = $this->where('id',$uid)->first();
        if(!$user){
            return false;
        }
        $user->password = Hash::make($password);
        $status = $user->update();
        return $status;
    }

    public function regenerate_api($uid){
        $user = $this->where('id',$uid)->first();
        if(!$user){
            return false;
        }
        $apiSecret = Str::random(50);
        $user->api_secret = $apiSecret;
        $status = $user->update();
        return $status;
    }
}
