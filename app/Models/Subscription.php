<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'title' => $data['title'],
            'subscribed_date' => $data['start_date'],
            'end_date' => $data['end_date'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $subscription = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$subscription){
            return false;
        }
        $subscription->title = $data['title'];
        $subscription->subscribed_date = $data['start_date'];
        $subscription->end_date = $data['end_date'];
        $status = $subscription->update();
        return $status; 
    }

    public function delete_data($id,$uid){
        $subscription = $this->where('id',$id)->where('user_id',$uid)->first(); 
        if(!$subscription){
            return false;
        }
        $status = $subscription->delete();
        return $status;   
    }
}
