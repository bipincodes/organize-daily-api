<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'title' => $data['title'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $todo = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$todo){
            return false;
        }
        $todo->title = $data['title'];
        $status = $todo->update();
        return $status;
    }

    public function delete_data($id,$uid){
        $todo = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$todo){
            return false;
        }
        $status = $todo->delete();
        return $status;
    }

    public function check_todo($id,$uid){
        $todo = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$todo){
            return false;
        }
        if($todo->task_done == 0){
            $todo->task_done = 1;
        }else{
            $todo->task_done = 0;
        }
        $status = $todo->update();
        return $status; 
    }

}
