<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'item' => $data['item'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $shopping = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$shopping){
            return false;
        }
        $shopping->item = $data['item'];
        $shopping->is_bought = $data['is_bought'];
        $status = $shopping->update();
        return $status;  
    }

    public function delete_data($id,$uid){
        $shopping = $this->where('id',$id)->where('user_id',$uid)->first(); 
        if(!$shopping){
            return false;
        }
        $status = $shopping->delete();
        return $status;
    }

    public function tick_untick_shopping_list($id,$uid){
        $shopping = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$shopping){
            return false;
        }
        if($shopping->is_bought == 0){
            $shopping->is_bought = 1;
        }else{
            $shopping->is_bought = 0;
        }
        $status = $shopping->update();
        return $status;
    }
}
