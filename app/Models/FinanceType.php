<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinanceType extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'type' => $data['type'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $financeType = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$financeType){
            return false;
        }
        $financeType->type = $data['type'];
        $status = $financeType->update();
        return $status;
    }

    public function delete_data($id,$uid){
        $financeType = $this->where('id',$id)->where('user_id',$uid)->first(); 
        if(!$financeType){
            return false;
        }
        $status = $financeType->delete();
        return $status;
    }
}
