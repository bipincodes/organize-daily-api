<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'user_id' => $data['user_id'],
            'feedback' => $data['feedback'],
        ]);
        if($status->id){
            return ['status'=> true,'id' => $status->id];
        }
        return ['status'=> false, 'api_secret' => null];
    }
}
