<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        $status = $this->create([
            'key' => $data['key'],
            'value' => $data['value'],
        ]);
        if($status->id){
            return ['status'=> true, 'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($data){
        $setting = $this->where('id',$data['id'])->first();
        if(!$setting){
            return false;
        }
        $setting->value = $data['value'];
        $status = $setting->update();
        return $status;  
    }

    public function delete_data($id){
        $setting = $this->where('id',$id)->first();
        if(!$setting){
            return false;
        }
        $status = $setting->delete();
        return $status;  
    }
}
