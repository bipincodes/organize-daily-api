<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    use HasFactory;
    protected $guarded = "";

    public function add_data($data){
        if(!array_key_exists('tag',$data)){
            $tag = null;
        }else{
            $tag = $data['tag'];
        }
        $status = $this->create([
            'user_id' => $data['user_id'],
            'reason' => $data['reason'],
            'amount' => $data['amount'],
            'type' => $data['type'],
            'tag_id' => $tag
            
        ]);
        if($status->id){
            return ['status'=> true,'id' => $status->id];
        }
        return ['status'=> false, 'id' => null];
    }

    public function update_data($id,$data,$uid){
        $finance = $this->where('id',$id)->where('user_id',$uid)->first();
        if(!$finance){
            return false;
        }
        $finance->reason = $data['reason'];
        $finance->amount = $data['amount'];
        $finance->type = $data['type'];
        if(array_key_exists('tag',$data)){
            $finance->tag_id = $data['tag'];
        }
        $status = $finance->update();
        return $status;
    }

    public function delete_data($id,$uid){
        $finance = $this->where('id',$id)->where('user_id',$uid)->first(); 
        if(!$finance){
            return false;
        }
        $status = $finance->delete();
        return $status;
    }
}
