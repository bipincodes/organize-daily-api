<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\EmailListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front');
});

Route::get('unsubscribe-email/{email}', [EmailListController::class,'deleteEmailList']);
Route::post('email-subscribe', [EmailListController::class,'addEmailFromSystem']);
