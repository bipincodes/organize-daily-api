<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\APIController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PremiumController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\ShoppingController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\EmailListController;
use App\Http\Controllers\WhatsWhereController;
use App\Http\Controllers\FinanceTypeController;
use App\Http\Controllers\FinancesTagController;
use App\Http\Controllers\SubscriptionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('yo', [APIController::class,'yo']);
Route::post('register', [UserController::class,'registerUser']);
Route::post('verify-account', [UserController::class,'verifyAccount']);
Route::post('login', [UserController::class,'loginUser']);
Route::post('email-subscribe', [EmailListController::class,'addEmailList']);
Route::get('get-va-version', [APIController::class,'getVAVersion']);


Route::middleware([isVerified::class])->group(function(){
    Route::middleware(isPremiumActivated::class)->group(function(){
        Route::resource('todo', TodoController::class);
        Route::resource('whats-where', WhatsWhereController::class);
        Route::resource('subscription', SubscriptionController::class);
        Route::resource('shopping', ShoppingController::class);
        Route::get('buy-item/{shopping}', [ShoppingController::class,'tickItem']);
        Route::post('feedback', [FeedbackController::class,'postFeedback']);
        Route::resource('finances-tag', FinancesTagController::class);
        Route::resource('finance', FinanceController::class);
    });
});

Route::middleware([isAdmin::class])->group(function(){
    Route::resource('finance-type', FinanceTypeController::class);
    Route::post('renew-premium/{user}', [PremiumController::class,'renewPremium']);
    Route::get('premium', [PremiumController::class,'getPremium']);
    Route::resource('settings', SettingsController::class);  
});