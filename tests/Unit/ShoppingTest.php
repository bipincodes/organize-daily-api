<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\ShoppingList;

class ShoppingTest extends TestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_create_shopping_list(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'item' => "Coffee",  
        ];
        $shopping = new ShoppingList;
        $shopping_data = $shopping->add_data($data);
        $this->assertTrue($shopping_data['status']);
    }

    /** @test */
    public function a_user_can_edit_shopping_list(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'item' => "Coffee",  
        ];
        $shopping = new ShoppingList;
        $shopping_data = $shopping->add_data($data);

        $new_data = [
            'item' => 'Tea',
            'is_bought' => 1
        ];
        $update_shopping = $shopping->update_data($shopping_data['id'],$new_data,$data['user_id']);
        $this->assertTrue($update_shopping);
    }

    /** @test */
    public function a_user_can_delete_shopping_list(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'item' => "Coffee",  
        ];
        $shopping = new ShoppingList;
        $shopping_data = $shopping->add_data($data);

        $delete_shopping = $shopping->delete_data($shopping_data['id'],$data['user_id']);
        $this->assertTrue($delete_shopping);
    }

    /** @test */
    public function a_user_can_check_uncheck_shopping_list(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'item' => "Coffee",  
        ];
        $shopping = new ShoppingList;
        $shopping_data = $shopping->add_data($data);

        $tick_shopping_item = $shopping->tick_untick_shopping_list($shopping_data['id'],$data['user_id']);
        $this->assertTrue($tick_shopping_item);
    }
}
