<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\TodoList;

class TodoTest extends TestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_create_todo(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'This is the first todo',
        ];
        $toDo = new TodoList;
        $newToDo = $toDo->add_data($data);
        $this->assertTrue($newToDo['status']);
    }

    /** @test */
    public function a_user_can_edit_todo(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'This is the first todo',
        ];
        $toDo = new TodoList;
        $newToDo = $toDo->add_data($data);
        $new_data = [
            'title' => 'Second Todo List',
        ];
        $updateToDo = $toDo->update_data($newToDo['id'],$new_data,$data['user_id']);
        $this->assertTrue($updateToDo);
    }

    /** @test */
    public function a_user_can_delete_todo(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'This is the first todo'
        ];
        $toDo = new TodoList;
        $newToDo = $toDo->add_data($data);
        $deleteToDo = $toDo->delete_data($newToDo['id'],$data['user_id']);
        $this->assertTrue($deleteToDo);
    }

    /** @test */
    public function a_user_can_check_mark_todo(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'This is the first todo'
        ];
        $toDo = new TodoList;
        $newToDo = $toDo->add_data($data);
        $checkToDo = $toDo->check_todo($newToDo['id'],$data['user_id']);
        $this->assertTrue($checkToDo);
    }
}
