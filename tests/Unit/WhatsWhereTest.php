<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\WhatsWhere;

class WhatsWhereTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function a_user_can_create_whats_where(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'what' => 'This is the first todo',
            'where' => 'This is the first todo',
        ];
        $whatswhere = new WhatsWhere;
        $newwhatswhere = $whatswhere->add_data($data);
        $this->assertTrue($newwhatswhere['status']);
    }
    /** @test */
    public function a_user_can_edit_whats_where(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'what' => 'This is the first todo',
            'where' => 'This is the first todo',
        ];
        $whatswhere = new WhatsWhere;
        $newwhatswhere = $whatswhere->add_data($data);

        $new_data = [
            'what' => 'This is the first todo',
            'where' => 'This is the first todo',
        ];
        $updateToDo = $whatswhere->update_data($newwhatswhere['id'],$new_data,$data['user_id']);
        $this->assertTrue($updateToDo);
    }

    /** @test */
    public function a_user_can_delete_whats_where(){
        $userID = create_a_dummy_user();
        $data = [
            'what' => 'Dom Dom',
            'where' => 'Purse ko kuna ma',
            'user_id' => $userID,
        ];
        $whatsWhere = new WhatsWhere;
        $newwhatsWhere = $whatsWhere->add_data($data);

        $deleteWhatsWhere = $whatsWhere->delete_data($newwhatsWhere['id'],$data['user_id']);
        $this->assertTrue($deleteWhatsWhere);
    }
}