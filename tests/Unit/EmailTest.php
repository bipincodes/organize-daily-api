<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\EmailList;

class EmailTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_subscribe_email_list(){
        $data = [
            'email' => 'bipincodes@gmail.com'
        ];
        $emailList = new EmailList;
        $new_emailList = $emailList->add_data($data);
        $this->assertTrue($new_emailList['status']);
    }

    /** @test */
    public function a_user_can_unsubscribe_email_list(){
        $data = [
            'email' => 'bipincodes@gmail.com'
        ];
        $emailList = new EmailList;
        $new_emailList = $emailList->add_data($data);
        $delete_emailList = $emailList->delete_data($data);
        $this->assertTrue($delete_emailList);
    }
}
