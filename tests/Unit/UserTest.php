<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class UserTest extends TestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_create_an_account(){
        $data = [
            'name' => 'Organize Daily',
            'nickname' => 'Sir',
            'email' => 'hello.organizedaily@gmail.com',
            'phone' => '9860119162',
            'password' => 'wearedope!',
            'confirm_password' => 'wearedope!',
        ];
        $user = new User;
        $newUser = $user->add_data($data); 
        $this->assertTrue($newUser['status']);
    }

    /** @test */
    public function a_user_can_change_a_password(){
        $userID = create_a_dummy_user();
        $user = new User;
        $changePassword = $user->change_password($userID,'isthisworking?');
        $this->assertTrue($changePassword);
    }

    /** @test */
    public function a_user_can_regenerate_an_api_key(){
        $userID = create_a_dummy_user();
        $user = new User;
        $regenerateAPI = $user->regenerate_api($userID);
        $this->assertTrue($regenerateAPI);
    }
}
