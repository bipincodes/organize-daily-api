<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\Settings;

class SettingsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function an_admin_can_add_settings(){
        $data = [
            'key' => 'key1',  
            'value' => 'value1'
        ];
        $setting = new Settings;
        $setting_data = $setting->add_data($data);
        $this->assertTrue($setting_data['status']);
    }

    /** @test */
    public function an_admin_can_edit_settings(){
        $data = [
            'key' => 'key1',  
            'value' => 'value1'
        ];
        $setting = new Settings;
        $setting_data = $setting->add_data($data);

        $newData = [
            'id' => $setting_data['id'],
            'value' => 'value2'
        ];
        $editSetting = $setting->update_data($newData);
        $this->assertTrue($editSetting);
    }

    /** @test */
    public function an_admin_can_delete_settings(){
        $data = [
            'key' => 'key1',  
            'value' => 'value1'
        ];
        $setting = new Settings;
        $setting_data = $setting->add_data($data);
        $deleteSetting = $setting->delete_data($setting_data['id']);
        $this->assertTrue($deleteSetting);
    }
}
