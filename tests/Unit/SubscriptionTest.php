<?php

namespace Tests\Unit;

use App\Models\Subscription;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SubscriptionTest extends TestCase
{
    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_create_subscription(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'PMS Subscription',
            'start_date' => '2023-01-17',
            'end_date' => '2024-01-14'
        ];
        $subscription = new Subscription();
        $new_subscription = $subscription->add_data($data);
        $this->assertTrue($new_subscription['status']);
    }

    /** @test */
    public function a_user_can_edit_subscription(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'PMS Subscription',
            'start_date' => '2023-01-17',
            'end_date' => '2024-01-14'
        ];
        $subscription = new Subscription;
        $new_subscription = $subscription->add_data($data);

        $new_data = [
            'title' => 'PMS Subscription Edit',
            'start_date' => '2023-01-15',
            'end_date' => '2023-02-15'
        ];
        $update_subscription = $subscription->update_data($new_subscription['id'],$new_data,$data['user_id']);
        $this->assertTrue($update_subscription);
    }

    /** @test */
    public function a_user_can_delete_subscription(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'title' => 'PMS Subscription',
            'start_date' => '2023-01-17',
            'end_date' => '2024-01-14'
        ];
        $subscription = new Subscription();
        $new_subscription = $subscription->add_data($data);

        $delete_subscription = $subscription->delete_data($new_subscription['id'],$data['user_id']);
        $this->assertTrue($delete_subscription);
    }

    public function a_user_can_renew_subscription(){

    }
}
