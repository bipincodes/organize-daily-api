<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use Illuminate\Support\Carbon;

use App\Models\PremiumPlan;

class PremiumTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_system_can_add_trial_plan(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'start_date' => Carbon::now(),  
            'end_date' => Carbon::now()->addDay(3),  
        ];
        $premium = new PremiumPlan;
        $premium_data = $premium->add_data($data);
        $this->assertTrue($premium_data['status']);
    }

    
    /** @test */
    public function a_system_can_renew_premium_plan(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'start_date' => Carbon::now(),  
            'end_date' => Carbon::now()->addYear(),  
        ];
        $premium = new PremiumPlan;
        $premium_data = $premium->add_data($data);
        $this->assertTrue($premium_data['status']);
    }
}
