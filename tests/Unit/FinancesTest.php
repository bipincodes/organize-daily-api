<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\Finance;
use App\Models\FinancesTag;
use App\Models\FinanceType;

class FinancesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_user_can_create_tags(){
        $userID = create_a_dummy_user();
        $data =[
            'user_id' => $userID,
            'name' => 'First Tag',
        ];
        $tag = new FinancesTag;
        $new_tag = $tag->add_data($data);
        $this->assertTrue($new_tag['status']);
    }

    /** @test */
    public function a_user_can_edit_tags(){
        $userID = create_a_dummy_user();
        $data =[
            'user_id' => $userID,
            'name' => 'First Tag',
            'slug' => 'first-tag'
        ];
        $tag = new FinancesTag;
        $new_tag = $tag->add_data($data);

        $new_data = [
            'name' => 'Second Tag',
            'slug' => 'second-tag'
        ];
        $update_data = $tag->update_data($new_tag['id'],$new_data,$data['user_id']);
        $this->assertTrue($update_data);
    }

    /** @test */
    public function a_user_can_delete_tags(){
        $userID = create_a_dummy_user();
        $data =[
            'user_id' => $userID,
            'name' => 'First Tag',
            'slug' => 'first-tag'
        ];
        $tag = new FinancesTag;
        $new_tag = $tag->add_data($data);

        $delete_tag = $tag->delete_data($new_tag['id'],$data['user_id']);
        $this->assertTrue($delete_tag);
    }

    /** @test */
    public function a_user_can_create_entry(){
        $userID = create_a_dummy_user();
        $data =[
            'user_id' => $userID,
            'reason' => 'Kharcha',
            'amount' => 5000,
            'type' => 1,
        ];
        $finance = new Finance;
        $newRecord = $finance->add_data($data);
        $this->assertTrue($newRecord['status']);
    }
    /** @test */
    public function a_user_can_edit_entry(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'reason' => 'Kharcha',
            'amount' => 5000,
            'type' => 1,
        ];
        $finance = new Finance;
        $newRecord = $finance->add_data($data);
        $new_data = [
            'reason' => 'Kharcha',
            'amount' => 5000,
            'type' => 2,
            'tag' => 1
        ];
        $updateFinance = $finance->update_data($newRecord['id'],$new_data,$data['user_id']);
        $this->assertTrue($updateFinance);
    }

    /** @test */
    public function a_user_can_delete_entry(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'reason' => 'Kharcha',
            'amount' => 5000,
            'type' => 1,
        ];
        $finance = new Finance;
        $newRecord = $finance->add_data($data);
        
        $deleteRecord = $finance->delete_data($newRecord['id'],$data['user_id']);
        $this->assertTrue($deleteRecord);
    }

    /** @test */
    public function a_system_can_add_finance_type(){
        $userID = create_a_dummy_user();
        $data =[
            'user_id' => $userID,
            'type' => 'Kharcha',
        ];
        $finance = new FinanceType;
        $newRecord = $finance->add_data($data);
        $this->assertTrue($newRecord['status']);
    }

    /** @test */
    public function a_system_can_edit_finance_type(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'type' => 'Expense',
        ];
        $finance = new FinanceType;
        $newRecord = $finance->add_data($data);
        $new_data = [
            'type' => 'Expensess',
        ];
        $updateFinance = $finance->update_data($newRecord['id'],$new_data,$data['user_id']);
        $this->assertTrue($updateFinance);
    }

    /** @test */
    public function a_system_can_delete_finance_type(){
        $userID = create_a_dummy_user();
        $data = [
            'user_id' => $userID,
            'type' => 'Expense',
        ];
        $financeType = new FinanceType;
        $newRecord = $financeType->add_data($data);
        
        $deleteRecord = $financeType->delete_data($newRecord['id'],$data['user_id']);
        $this->assertTrue($deleteRecord);
    }


}
