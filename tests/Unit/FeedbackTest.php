<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\Feedback;

class FeedbackTest extends TestCase
{

    use DatabaseMigrations;
    
    /** @test */
    public function a_user_can_write_feedback(){
        $userID = create_a_dummy_user();
        $data =[
            'user_id' => $userID,
            'feedback' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam bibendum eleifend tellus. Curabitur luctus sem vel dui lobortis viverra. Sed vestibulum, eros vel pretium molestie, ex enim pellentesque nibh, ut tempor sem est sit amet sapien. In velit arcu, convallis gravida neque sit amet, euismod interdum lacus. Nullam imperdiet a mi in rhoncus. Duis in nisi ac dolor porta blandit. Nulla vel viverra ipsum, eu hendrerit velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac lacus suscipit, laoreet urna et, fermentum libero. Proin non posuere leo. Etiam sagittis ipsum nec ipsum aliquet feugiat. Cras a massa id arcu tristique vulputate. Sed a erat nulla. Maecenas a rhoncus neque, vitae posuere dolor. Etiam accumsan aliquam nulla, eu interdum mauris lobortis ut. Nulla sed feugiat purus.',
        ];
        $feedback = new Feedback;
        $new_feedback = $feedback->add_data($data);
        $this->assertTrue($new_feedback['status']);
    }
}
